<?php


namespace App\Http\Controllers;



use App\Serie;
use App\Services\CriadorSerie;
use App\Services\RemovedorSerie;
use Illuminate\Http\Request;

class SeriesController extends Controller
{
    public function index(Request $request) {
        $series = Serie::query()->orderBy('id')->get();

        $mensagem = $request->session()->get('mensagem');


        return view('series.index', compact('series', 'mensagem'));
    }
    public function create(){
        return view('series.create');
    }

    public function store(Request $request, CriadorSerie $criadorSerie)
    {
        $request->validate([
            'nome' => 'required',
            'qtd_temporada' => 'required',
            'ep_por_temporada' => 'required'
        ]);

        $serie = $criadorSerie->criar_serie($request->nome, $request->qtd_temporada, $request->ep_por_temporada);

        $request->session()->flash(
          'mensagem', "{$serie->nome} adicionado com sucesso"
        );

        return redirect('/');
    }

    public function editaNome(int $id,Request $request)
    {
        $novoNome = $request->nome;
        $serie = Serie::find($id);
        $serie->nome = $novoNome;
        $serie->save();
    }

    public function destroy(Request $request, RemovedorSerie $removedorSerie)
    {
        $nome_serie = $removedorSerie->remover_serie($request->id);

        $request->session()->flash(
            'mensagem', "Série $nome_serie foi removida com sucesso"
        );
        return redirect('/');
    }
}