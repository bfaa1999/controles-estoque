<?php


namespace App\Http\Controllers;


use App\Serie;
use App\Services\RemovedorTemporada;
use Illuminate\Http\Request;

class TemporadasController extends Controller
{
    public function index(int $serie_id, Request $request)
    {
        $serie = Serie::find($serie_id);
        $temporadas = $serie->temporadas;

        $mensagem = $request->session()->get('mensagem');

        return view('temporadas.index', compact('temporadas','serie', 'mensagem'));
    }

    public function destroy(int $temporadaId, Request $request, RemovedorTemporada $removedorTemporada)
    {
        $numero_temporada = $removedorTemporada->remover_temporada($temporadaId);

        $request->session()->flash(
            'mensagem', "Temporada {$numero_temporada} foi removida com sucesso"
        );

        return redirect()->back();
    }
}