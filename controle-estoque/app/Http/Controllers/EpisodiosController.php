<?php


namespace App\Http\Controllers;


use App\Episodio;
use App\Services\CriadorEpisodio;
use App\Services\RemovedorEpisodio;
use App\Temporada;
use Illuminate\Http\Request;

class EpisodiosController extends Controller
{
    public function index(Temporada $temporada, Request $request)
    {
        $episodios = $temporada->episodios;
        $temporadaID = $temporada->id;

        $mensagem = $request->session()->get('mensagem');

        return view('episodios.index', compact('episodios', 'temporadaID', 'mensagem'));
    }

    public function store(int $temporadaId, Request $request, CriadorEpisodio $criadorEpisodio)
    {
        $request->validate([
            'numero_episodio' => 'required'
        ]);

        $criadorEpisodio->criar_episodio($request->numero_episodio, $temporadaId);

        $request->session()->flash(
            'mensagem', "Episódio {$request->numero_episodio} adicionado com sucesso"
        );
        return redirect()->back();
    }

    public function assistir(int $episodioId)
    {
        $episodioAssistido = Episodio::find($episodioId);
        if($episodioAssistido->assistido){
            $episodioAssistido->assistido = false;
        }
        else{
            $episodioAssistido->assistido = true;
        }
        $episodioAssistido->save();

        return redirect()->back();
    }

    public function destroy(Request $request, RemovedorEpisodio $removedorEpisodio)
    {
        $nome_episodio = $removedorEpisodio->remover_episodio($request->episodioId);

        $request->session()->flash(
            'mensagem',
            "$nome_episodio removido com sucesso!"
        );
        return redirect()->back();
    }
}