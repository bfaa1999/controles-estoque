<?php


namespace App\Services;


use App\Episodio;
use App\Temporada;
use Illuminate\Support\Facades\DB;

class RemovedorTemporada
{
    public function remover_temporada(int $temporadaId): int
    {
        DB::beginTransaction();
        $temporada_delete = Temporada::find($temporadaId);
        $numero_ep = $temporada_delete->numero;

        $this->remover_epi($temporada_delete);

        $temporada_delete->delete();
        DB::commit();

        return $numero_ep;
    }

    public function remover_epi($temporada)
    {
        $temporada->episodios->each(function (Episodio $episodio) {
           $episodio->delete();
        });
    }
}