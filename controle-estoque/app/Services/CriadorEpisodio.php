<?php


namespace App\Services;


use App\Episodio;
use App\Temporada;
use Illuminate\Support\Facades\DB;

class CriadorEpisodio
{
    public function criar_episodio(int $epi_numero, int $temporadaId): void
    {
        DB::beginTransaction();
        $temporada = Temporada::find($temporadaId);
        $temporada->episodios()->create(['numero' => $epi_numero]);
        DB::commit();
    }
}