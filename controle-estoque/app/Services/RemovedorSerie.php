<?php


namespace App\Services;


use App\Episodio;
use App\Serie;
use App\Temporada;
use Illuminate\Support\Facades\DB;

class RemovedorSerie
{
    public function remover_serie(int $serieId): string
    {
        DB::beginTransaction();
        $serie = Serie::find($serieId);
        $nome_serie = $serie->nome;
        $this->remover_temporada($serie);

        $serie->delete();
        DB::commit();

        return $nome_serie;
    }

    public function remover_temporada($serie): void
    {
        $serie->temporadas->each(function (Temporada $temporada){
            $this->remover_episodio($temporada);
            $temporada->delete();
        });
    }

    public function remover_episodio($temporada): void
    {
        $temporada->episodios->each(function (Episodio $episodio) {
            $episodio->delete();
        });
    }
}
