<?php


namespace App\Services;


use App\Episodio;
use Illuminate\Support\Facades\DB;

class RemovedorEpisodio
{
    public function remover_episodio(int $episodioID): string
    {
        DB::beginTransaction();
        $episodio_delete = Episodio::find($episodioID);
        if($episodio_delete->nome == null){
            $episodio_delete->nome = "Episodio $episodio_delete->numero";
        }
        $nome_episodio = $episodio_delete->nome;

        $episodio_delete->delete();
        DB::commit();

        return $nome_episodio;
    }
}