<?php


namespace App\Services;


use App\Serie;
use Illuminate\Support\Facades\DB;

class CriadorSerie
{
    public function criar_serie(string $nome_serie, int $qtd_temporada, int $ep_por_temporada):Serie
    {
        DB::beginTransaction();
        $serie = Serie::create(['nome' => $nome_serie]);
        for($i = 1; $i <= $qtd_temporada; $i++){
            $temporada = $serie->temporadas()->create(['numero' => $i]);

            for($j = 1; $j <= $ep_por_temporada; $j++){
                $temporada->episodios()->create(['numero' => $j]);
            }
        }
        DB::commit();

        return $serie;
    }
}