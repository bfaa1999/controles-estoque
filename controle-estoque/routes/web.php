<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/', 'SeriesController@index');

Route::get('/criar', 'SeriesController@create')->name('form-criar-serie');

Route::post('/criar', 'SeriesController@store');

Route::delete('/remover/{id}', 'SeriesController@destroy');

Route::post('/series/{id}/editaNome', 'SeriesController@editaNome');

Route::get('/series/{serie_id}/temporadas', 'TemporadasController@index');

Route::delete('/temporada/{temporadaId}/remover', 'TemporadasController@destroy');

Route::get('/temporada/{temporada}/episodios', 'EpisodiosController@index');

Route::post('/episodio/{episodioId}/assistir', 'EpisodiosController@assistir');

Route::post('/temporada/{temporadaId}/episodio/criar', 'EpisodiosController@store')->name('form-criar-episodio');

Route::delete('/temporada/{temporadaId}/episodios/{episodioId}/remover', 'EpisodiosController@destroy');

Auth::routes();
