@extends('layout')

@section('cabecalho')
Adicionar Série
@endsection

@section('conteudo')
@include('subview.erros')
    <form method="post">
        @csrf
        <div class="row align-items-center">
            <div class="col col-8">
                <label for="nome" class="">Nome</label>
                <input type="text" class="form-control" name="nome" id="nome">
            </div>

            <div class="col col-2">
                <label for="qtd_temporada" class="text-justify">Nº Temporadas</label>
                <input type="number" min="1" class="form-control" name="qtd_temporada" id="qtd_temporada">
            </div>

            <div class="col col-2">
                <label for="ep_por_temporada" class="">Ep. por temporada</label>
                <input type="number" min="1" class="form-control" name="ep_por_temporada" id="ep_por_temporada">
            </div>

            <button class="btn btn-primary btn-lg mt-2 ml-3">
                <i class="far fa-save"></i> Salvar
            </button>
        </div>
    </form>
@endsection

