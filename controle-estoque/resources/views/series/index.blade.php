@extends('layout')

@section('cabecalho')
    Séries
@endsection

@section('conteudo')
    @include('subview.mensagem')

    <a href="{{ route('form-criar-serie') }}" class="btn btn-danger mb-2">
        <i class="fas fa-plus-circle"></i> Novo
    </a>

    <!-- TABELA DE SÉRIES-->

    <table class="table" id="table-series">

        <thead>
            <tr>
                <th style="text-align: center; width: 4%">#</th>
                <th style="text-align: center; width: 45%">Nome</th>
                <th style="text-align: center; width: 16%">Nº Temporadas</th>
                <th style="text-align: center; width: 30%">Opções</th>
            </tr>
        </thead>

        <tbody>

            @foreach($series as $serie)
                <tr>
                    <!--#######-->
                    <td style="text-align: center; width: 4%">{{ $serie->id }}</td>
                    <!--###NOME###-->
                    <td  style="text-align: center; width: 45%">
                        <span id="nome-serie-{{ $serie->id }}">{{ $serie->nome }}</span>

                        <div class="input-group w-50" hidden id="input-nome-serie-{{ $serie->id }}">
                            <input type="text" class="form-control" value="{{ $serie->nome }}"/>
                            <div class="input-group-append">
                                <button class="btn btn-primary" onclick="editarSerie({{ $serie->id }})">
                                    <i class="fas fa-check"></i>
                                    @csrf
                                </button>
                            </div>
                        </div>
                    </td >
                    <!--###Nº TEMPORADAS###-->
                    <td style="text-align: center; width: 26%">
                        <h5>
                            <span class="badge badge-primary">
                                {{ $serie->temporadas()->count() }}
                            </span>
                        </h5>
                    </td>
                    <!--###OPÇÕES###-->
                    <td style="text-align: center; width: 30%">
                        <span class="d-flex">
                            <button class="btn btn-info btn-sm mr-1" onclick="toggleInput({{ $serie->id }})">
                                <i class="fas fa-edit"></i> Editar
                            </button>
                            <a href="/series/{{ $serie->id }}/temporadas" class="btn btn-info btn-sm mr-1">
                                <i class="fas fa-external-link-alt"></i> Abrir
                            </a>
                            <form method="post" action="/remover/{{ $serie->id }}"
                                  onsubmit="return confirm('Tem certeza que deseja excluir {{ $serie->nome }}?')">
                                @csrf
                                @method('DELETE')
                                <button class="btn btn-danger btn-sm">
                                    <i class="far fa-trash-alt"></i> Apagar
                                </button>
                            </form>
                        </span>
                    </td>
                </tr>
            @endforeach

        </tbody>
    </table>
    <script>
        $(document).ready(function() {
            $('#table-series').DataTable();
        } );

        function toggleInput(serieId) {
            const inputSerieEl = document.getElementById(`input-nome-serie-${serieId}`);
            const nomeSerieEl = document.getElementById(`nome-serie-${serieId}`);
            if(nomeSerieEl.hasAttribute('hidden')){
                nomeSerieEl.removeAttribute('hidden');
                inputSerieEl.hidden = true;
            } else {
                nomeSerieEl.hidden = true;
                inputSerieEl.removeAttribute('hidden');
            }
        }
        
        function editarSerie(serieId) {
            let formData = new FormData();

            const novo_nome = document.querySelector(`#input-nome-serie-${serieId} > input`).value;
            const url = `/series/${serieId}/editaNome`;
            const token = document.querySelector('input[name = "_token"]').value;
            formData.append('nome', novo_nome);
            formData.append('_token', token);


            fetch(url, {
                body: formData,
                method: 'POST'
            }).then(() => {
                toggleInput(serieId);
                document.getElementById(`nome-serie-${serieId}`).textContent = novo_nome;
            });
        }
    </script>
@endsection