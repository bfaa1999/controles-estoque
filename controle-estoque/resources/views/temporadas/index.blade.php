@extends('layout')

@section('cabecalho')
{{ $serie->nome }}
@endsection

@section('conteudo')
    @include('subview.mensagem')

    <!-- TABELA TEMPORADAS-->
    <table class="table">
        <thead class="thead-dark">
        <tr>
            <th>#</th>
            <th>Temporada</th>
            <th>Assistidos</th>
            <th>Remover</th>
        </tr>
        </thead>
        <tbody>
        @foreach($temporadas as $temporada)
            <tr>
                <!--#######-->
                <td>{{ $temporada->id }}</td>
                <!--###TEMPORADA###-->
                <td><a href="/temporada/{{ $temporada->id }}/episodios">Temporada {{$temporada->numero}}</a></td>
                <!--###ASSISTIDOS###-->
                <td>
                        <span class="badge badge-secondary">
                            {{ $temporada->getEpisodiosAssistidos()->count() }} / {{ $temporada->episodios->count() }}
                        </span>
                </td>
                <!--###REMOVER###-->
                <td>
                    <form action="/temporada/{{ $temporada->id }}/remover" method="post"
                          onclick="return confirm('Tem certeza que deseja excluir essa temporada')">
                        @csrf
                        @method('DELETE')
                        <button class="btn btn-danger btn-sm ml-2">
                            <i class="far fa-trash-alt"></i> Apagar
                        </button>
                    </form>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection