@extends('layout')

@section('cabecalho')
Episódios
@endsection

@section('conteudo')
    @include('subview.erros')
    @include('subview.mensagem')
    <h1 id="template-photo1">OLA MENINAS</h1>
    <script>
        duplicador([0, 1, 3, 4, 5, 6, 7]);
    </script>
    <button class="btn btn-success mb-2" onclick="document.getElementById('input-nome-episodio').removeAttribute('hidden')">
        <i class="fas fa-plus-circle"></i> Novo
    </button>

    <form action="/temporada/{{ $temporadaID }}/episodio/criar" method="post">
        <div class="input-group w-25" hidden id="input-nome-episodio">
            @csrf
            <label for="numero_episodio">
                <input type="number" min="1" class="form-control" name="numero_episodio" id="numero_episodio" placeholder="Qual o número do episódio?"/>
            </label>
            <div class="input-group-append">
                <button class="btn btn-primary">
                    <i class="fas fa-check"></i>
                </button>
            </div>
        </div>
    </form>

    <!-- TABELA EPISÓDIOS -->

    <table class="table">
        <thead class="thead-dark">
            <tr>
                <th>#</th>
                <th>Episódio</th>
                <th>Situação</th>
                <th>Assistir</th>
                <th>Remover</th>
            </tr>
        </thead>
        <tbody>
            @foreach($episodios as $episodio)
                <tr>
                    <!--#######-->
                    <td>{{ $episodio->id }}</td>
                    <!--##EPISÓDIO###-->
                    <td>
                        <span id="nome-episodio-{{ $episodio->id }}">
                            Episódio {{ $episodio->numero }}
                        </span>
                    </td>
                    <!--###SITUAÇÃO###-->
                    <td>
                        <h5>
                            <span id="badge-episodio-{{ $episodio->id }}" class="badge badge-warning badge-pill mr-2">
                                Não Assistido
                            </span>
                        </h5>
                    </td>
                    <!--###ASSISTIR###-->
                    <td>
                        <form action="/episodio/{{ $episodio->id }}/assistir" method="post" style="margin-bottom: 0">
                            @csrf
                            <button id="botao-assistir-{{ $episodio->id }}" class="btn btn-primary btn-sm">
                                <i id="icon-assistir-{{ $episodio->id }}" class="fas fa-check"></i>
                            </button>
                        </form>
                        @if($episodio->assistido)
                            <script>
                                alteraBadge({{ $episodio->id }});
                                alteraButton({{ $episodio->id }});
                            </script>
                        @endif
                    </td>
                    <!--###REMOVER###-->
                    <td>
                        <form action="/temporada/{{ $temporadaID }}/episodios/{{ $episodio->id }}/remover" method="post"
                              onclick="return confirm('Tem certeza de deseja excluir esse episódio?')" style="margin-bottom: 0">
                            @csrf
                            @method('DELETE')
                            <button class="btn btn-danger btn-sm ml-2">
                                <i class="far fa-trash-alt"></i> Apagar
                            </button>
                        </form>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>

@endsection

<script>
    function alteraBadge(id){
        document.getElementById(`badge-episodio-${id}`).className = "badge badge-success badge-pill mr-2";
        document.getElementById(`badge-episodio-${id}`).textContent = "Assistido";
    }

    function alteraButton(id) {
        document.getElementById(`botao-assistir-${id}`).className = "btn btn-secondary btn-sm";
        document.getElementById(`icon-assistir-${id}`).className = "far fa-times-circle";
    }



    function duplicador(titulo_photo){
        if(titulo_photo.length > 1){
            let original = document.getElementById("template-photo1");
            for(let i = 0; i < titulo_photo.length; i++){
                console.log("Teste" + i);
                let clone = original.cloneNode(true);
                clone.id = "template-photo" + i;
                original.parentNode.appendChild(clone);
            }
        }
    }
</script>
