function alteraBadge(id){
    document.getElementById(`badge-episodio-${id}`).className = "badge badge-success badge-pill mr-2";
    document.getElementById(`badge-episodio-${id}`).textContent = "Assistido";
}

function alteraButton(id) {
    document.getElementById(`botao-assistir-${id}`).className = "btn btn-secondary btn-sm";
    document.getElementById(`icon-assistir-${id}`).className = "far fa-times-circle";
}
